#pragma once

#include <iostream>
#include <unordered_map>
#include <vector>
#include "../macros.hpp"

namespace hft
{
    namespace Common
    {
        template <typename T>
        class Vector
        {
        public:
            struct value
            {
                T object_;
                bool is_valid{false};
            };

            static constexpr size_t INVALID_INDEX = static_cast<size_t>(-1);
            static constexpr size_t MAX_STORE_SIZE = 256;
            std::vector<value> data_;
            std::vector<size_t> invalid_indices_;

            Vector()
                : data_(256),
                  invalid_indices_(256)
            {
            }

            Vector(Vector &&v)
            {
                data_ = std::move(v.data_);
                invalid_indices_ = std::move(v.invalid_indices_);
            }

            Vector &operator=(Vector &&v)
            {
                this->data_ = std::move(v.data_);
                this->invalid_indices_ = std::move(v.invalid_indices_);
                return *this;
            }

            Vector(const Vector &) = delete;
            Vector &operator=(const Vector &) = delete;

            auto size() const
            {
                return data_.size();
            }

            auto capacity() const
            {
                return data_.capacity();
            }

            T &at(size_t index)
            {
                hft::Macros::ASSERT(index < size(), "Index out of Vector bounds");
                return data_[index].object_;
            }

            T &operator[](size_t idx)
            {
                value &v = data_[idx];
                v.is_valid = true;
                return v.object_;
            }

            void emplace_back(const T &v)
            {
                if (UNLIKELY(invalid_indices_.size() == 0))
                {
                    data_.emplace_back(value{v, true});
                    return;
                }

                else if (UNLIKELY(invalid_indices_.size() == MAX_STORE_SIZE))
                {
                    auto index = invalid_indices_[MAX_STORE_SIZE - 1];
                    if (index != INVALID_INDEX)
                        data_[index].object_ = v;
                    data_.emplace_back(value{v, true});
                    invalid_indices_.clear();
                    invalid_indices_.reserve(MAX_STORE_SIZE);
                    return;
                }

                else
                {
                    auto idx = invalid_indices_[invalid_indices_.size() - 1];
                    data_[idx].object_ = v;
                    invalid_indices_[invalid_indices_.size() - 1] = INVALID_INDEX;
                    return;
                }
            }

            size_t find(const T &v)
            {
                const value *dptr = data_.data();
                auto count = data_.size();
                for (auto i = 0ul; i < count; i++)
                {
                    const value &val = dptr[count];
                    if (val.is_valid && val.object_ == v)
                        return count;
                }

                return INVALID_INDEX;
            }

            void erase(size_t index)
            {
                if (index >= size())
                    return;
                data_[index].is_valid = false;
                invalid_indices_.emplace_back(index);
            }
        };

    } // namespace Common

}