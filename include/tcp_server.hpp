#pragma once
#include "time_utils.hpp"
#include "tcp_socket.hpp"
#include "optimized/Vector.hpp"
#include <ranges>

namespace hft
{
    namespace Common
    {
        constexpr size_t EPOLL_EVENTS_MAX_SIZE = 1024ul;
        struct TCPServer
        {
            int efd_ = -1;
            TCPSocket listener_socket_;
            Vector<TCPSocket *> sockets_, receive_sockets_,
                send_sockets_, disconnected_sockets_;
            epoll_event events_[EPOLL_EVENTS_MAX_SIZE];
            std::function<void(TCPSocket *, Nanos)> recv_callback_;
            std::function<void()> recv_finished_callback_;
            std::string time_str_;
            Logger &logger_;

            explicit TCPServer(Logger &logger)
                : listener_socket_(logger),
                  logger_(logger)
            {
                recv_callback_ = [this](auto socket, auto rx_time)
                {
                    defaultRecvCallback(socket, rx_time);
                };
                recv_finished_callback_ = [this]()
                {
                    defaultRecvFinishedCallback();
                };
            }

            TCPServer() = delete;
            TCPServer(const TCPServer &) = delete;
            TCPServer(const TCPServer &&) = delete;
            TCPServer &operator=(const TCPServer &) = delete;
            TCPServer &operator=(const TCPServer &&) = delete;

            void defaultRecvCallback(TCPSocket *socket, Nanos rx_time) noexcept
            {
                logger_.log("#:# #() # TCPServer::defaultRecvCallback() socket:# len:# rx:#\n",
                            __FILE__, __LINE__, __FUNCTION__, TimeUtils::getCurrentTimeStr(&time_str_), socket->fd_,
                            socket->next_rcv_valid_index_, rx_time);
            }

            void defaultRecvFinishedCallback()
            {
                logger_.log("#:# #() # TCPServer::defaultRecvFinishedCallback()\n", __FILE__,
                            __LINE__, __FUNCTION__, TimeUtils::getCurrentTimeStr(&time_str_));
            }

            auto destroy()
            {
                close(efd_);
                efd_ = -1;
                listener_socket_.destroy();
            }

            auto listen(const std::string &iface, int port)
            {
                destroy();
                efd_ = epoll_create(1);
                Macros::ASSERT(efd_ >= 0,
                               "epoll_create () failed error:" + std::string(std::strerror(errno)));
                Macros::ASSERT(listener_socket_.connect("", iface, port, true) >= 0,
                               "Listener socket failed to connect. iface: " + iface + " port: " + std::to_string(port) +
                                   " error: " + std::string(std::strerror(errno)));
                Macros::ASSERT(epoll_add(&listener_socket_), "epoll_ctl() failed. error:" + std::string(std::strerror(errno)));
            }

            bool epoll_add(TCPSocket *socket)
            {
                epoll_event ev{};
                ev.events = EPOLLET | EPOLLIN;
                ev.data.ptr = reinterpret_cast<void *>(socket);
                return (epoll_ctl(efd_, EPOLL_CTL_ADD, socket->fd_, &ev) != -1);
            }

            bool epoll_del(TCPSocket *socket)
            {
                return (epoll_ctl(efd_, EPOLL_CTL_ADD, socket->fd_, nullptr) != -1);
            }

            void del(TCPSocket *socket)
            {
                epoll_del(socket);
                auto sockIndx = sockets_.find(socket);
                auto recvSockIndx = receive_sockets_.find(socket);
                auto sendSockIndx = send_sockets_.find(socket);
                sockets_.erase(sockIndx);
                receive_sockets_.erase(recvSockIndx);
                send_sockets_.erase(sendSockIndx);
            }

            void poll()
            {
                const int max_events = 1 + sockets_.size();
                const auto INVALID_INDEX = receive_sockets_.INVALID_INDEX;
                for (auto s : disconnected_sockets_.data_) {
                    if (s.is_valid)
                        del(s.object_);
                }
                const int n = epoll_wait(efd_, events_, max_events, 0);
                bool have_new_connection = false;
                for (auto i{0}; i < n; ++i)
                {
                    epoll_event &ev = events_[i];
                    auto socket = reinterpret_cast<TCPSocket *>(ev.data.ptr);
                    if (ev.events & EPOLLIN)
                    {
                        if (socket == &listener_socket_)
                        {
                            logger_.log("#:# #() # EPOLLIN listener_socket:#\n", __FILE__, __LINE__, __FUNCTION__,
                                        TimeUtils::getCurrentTimeStr(&time_str_), socket->fd_);
                            have_new_connection = true;
                            continue;
                        }
                        logger_.log("#:# #() # EPOLLIN socket:#\n", __FILE__, __LINE__, __FUNCTION__,
                                    TimeUtils::getCurrentTimeStr(&time_str_), socket->fd_);
                        if (receive_sockets_.find(socket) == INVALID_INDEX)
                            receive_sockets_.emplace_back(socket);
                    }

                    if (ev.events & EPOLLOUT)
                    {
                        logger_.log("#:# #() # EPOLLOUT socket:#\n", __FILE__, __LINE__, __FUNCTION__,
                                    TimeUtils::getCurrentTimeStr(&time_str_), socket->fd_);
                        if (send_sockets_.find(socket) == INVALID_INDEX)
                            send_sockets_.emplace_back(socket);
                    }

                    if (ev.events & (EPOLLERR | EPOLLHUP))
                    {
                        logger_.log("#:# #() # EPOLLERR socket:#\n", __FILE__, __LINE__, __FUNCTION__,
                                    TimeUtils::getCurrentTimeStr(&time_str_), socket->fd_);
                        if (disconnected_sockets_.find(socket) ==   INVALID_INDEX)
                            disconnected_sockets_.emplace_back(socket);
                    }

                    while (have_new_connection)
                    {
                        logger_.log("#:# #() # have_new_connection\n", __FILE__, __LINE__, __FUNCTION__,
                                    TimeUtils::getCurrentTimeStr(&time_str_));
                        sockaddr_storage addr;
                        socklen_t addr_len = sizeof(addr);
                        int fd = accept(listener_socket_.fd_, reinterpret_cast<sockaddr *>(&addr), &addr_len);
                        if (fd == -1)
                            break;
                        Macros::ASSERT(setNonBlocking(fd) && setNoDelay(fd),
                                       "Failed to set non-blocking or no-delay on socket:" + std::to_string(fd));
                        logger_.log("#:# #() # accepted socket:#\n", __FILE__, __LINE__, __FUNCTION__,
                                    TimeUtils::getCurrentTimeStr(&time_str_), fd);

                        TCPSocket *socket = new TCPSocket(logger_);
                        socket->fd_ = fd;
                        socket->recv_callback_ = recv_callback_;
                        Macros::ASSERT(epoll_add(socket),
                                       "Unable to add socket.error:" + std::string(std::strerror(errno)));
                        if (sockets_.find(socket) == INVALID_INDEX)
                            receive_sockets_.emplace_back(socket);
                    }
                }
            }

            void sendAndRecv()
            {
                auto recv = false;
                for (auto &socket : receive_sockets_.data_)
                {
                    if (socket.is_valid && socket.object_->sendAndRecv())
                        recv = true;
                }
                if (recv)
                    recv_finished_callback_();
                for (auto socket : send_sockets_.data_) {
                    if (socket.is_valid)
                        socket.object_->sendAndRecv();
                }
            }
        };
    }

}