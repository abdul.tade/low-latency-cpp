#pragma once
#include <type_traits>
#include <concepts>

namespace hft {
    
template <typename T>
concept DefaultConstructible = requires {
    std::is_default_constructible<T>::value;
};

}