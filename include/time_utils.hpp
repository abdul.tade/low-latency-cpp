#pragma once
#include <string>
#include <chrono>
#include <ctime>

namespace hft
{
    namespace Common
    {
        using Nanos = int64_t;
        using ClockType = std::chrono::system_clock;
        constexpr Nanos NANOS_TO_MICROS = 1000;
        constexpr Nanos MICROS_TO_MILLIS = 1000;
        constexpr Nanos MILLIS_TO_SECS = 1000;
        constexpr Nanos NANOS_TO_MILLIS = NANOS_TO_MICROS * MICROS_TO_MILLIS;
        constexpr Nanos NANOS_TO_SECS = NANOS_TO_MILLIS * MILLIS_TO_SECS;

        struct TimeUtils
        {
            static inline auto getCurrentNanos() noexcept 
            {
                return std::chrono::duration_cast<std::chrono::nanoseconds>(
                    ClockType::now().time_since_epoch()).count();
            }

            static inline auto& getCurrentTimeStr(std::string* time_str)
            {
                const auto time = ClockType::to_time_t(ClockType::now());
                time_str->assign(ctime(&time));
                if (!time_str->empty())
                {
                    time_str->at(time_str->length()-1) = '\0';
                }
                return *time_str;
            }
        };
    } // namespace Common
    
} // namespace hft
