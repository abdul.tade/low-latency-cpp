#pragma once
#include <iostream>
#include <immintrin.h>

namespace hft
{
    constexpr size_t COPY_SIZE = sizeof(unsigned long long);
    void *optimized_memcpy(void *dest, const void *src, size_t n)
    {
        const unsigned long long *_src = reinterpret_cast<const unsigned long long *>(src);
        unsigned long long *_dest = reinterpret_cast<unsigned long long *>(dest);
        size_t data_remainder_count = n % COPY_SIZE;
        size_t copyable_data_count = (n / COPY_SIZE );

        auto destChar = reinterpret_cast<char*>(_dest+copyable_data_count);
        auto srcChar = reinterpret_cast<const char*>(_src+copyable_data_count);

       std::copy(_src, _src+copyable_data_count, _dest);
       std::copy(srcChar, srcChar+data_remainder_count, destChar);

        return dest;
    }
} // namespace hft