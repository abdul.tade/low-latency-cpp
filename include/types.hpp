#pragma once

#include <cstdint>
#include <limits>
#include <compare>
#include "macros.hpp"

namespace hft
{
    namespace Common
    {
        template <typename T>
        T abs(T x) {
            return x < 0 ? -x : x;
        }
        using OrderId = uint64_t;
        constexpr auto OrderId_INVALID = std::numeric_limits<OrderId>::max();
        inline std::string orderIdToString(OrderId order_id)
        {
            if (UNLIKELY(order_id == OrderId_INVALID))
            {
                return "INVALID";
            }
            return std::to_string(order_id);
        }

        using TickerId = uint32_t;
        constexpr auto TickerId_INVALID = std::numeric_limits<TickerId>::max();
        inline std::string tickerIdToString(TickerId ticker_id)
        {
            if (UNLIKELY(ticker_id == TickerId_INVALID))
                return "INVALID";
            return std::to_string(ticker_id);
        }

        using ClientId = uint32_t;
        constexpr auto ClientId_INVALID = std::numeric_limits<ClientId>::max();
        inline std::string clientIdToString(ClientId client_id)
        {
            if (UNLIKELY(client_id == ClientId_INVALID))
                return "INVALID";
            return std::to_string(client_id);
        }

#pragma pack(push, 1)
        struct Price
        {
            uint64_t intValue_ = std::numeric_limits<uint64_t>::max();
            int16_t exponent_ = std::numeric_limits<int16_t>::max();

            bool operator==(const Price &price) {
                return (intValue_ == price.intValue_) and (exponent_ == price.exponent_);
            }

            bool operator!=(const Price &price) {
                return !operator==(price);
            }

            std::string toString() const  {
                return "Price(intValue: " + std::to_string(intValue_) + 
                            ", exponent: " + std::to_string(exponent_) + ")";
            }
        };
#pragma pack(pop)
        auto Price_INVALID = Price{std::numeric_limits<uint64_t>::max(), std::numeric_limits<uint16_t>::max()};
        inline std::string priceToString(const Price &price)
        {
            if (UNLIKELY(price == Price_INVALID))
                return "INVALID";
            return price.toString();
        }

        using Qty = uint32_t;
        constexpr auto Qty_INVALID = std::numeric_limits<Qty>::max();
        inline auto qtyToString(Qty qty) -> std::string
        {
            if (UNLIKELY(qty == Qty_INVALID))
            {
                return "INVALID";
            }
            return std::to_string(qty);
        }

        using Priority = uint64_t;
        constexpr auto Priority_INVALID = std::numeric_limits<Priority>::max();
        inline auto priorityToString(Priority priority) -> std::string
        {
            if (UNLIKELY(priority == Priority_INVALID))
            {
                return "INVALID";
            }
            return std::to_string(priority);
        }

        enum class Side : int8_t
        {
            INVALID = 0,
            BUY = 1,
            SELL = -1
        };

        inline std::string sideToString(Side side)
        {
            switch (side)
            {
            case Side::BUY:
                return "BUY";
            case Side::SELL:
                return "SELL";
            case Side::INVALID:
                return "INVALID";
            default:
                return "UNKNOWN";
            }
        }

        constexpr size_t LOG_QUEUE_SIZE = 8 * 1024 * 1024;
        constexpr size_t ME_MAX_TICKERS = 8;
        constexpr size_t ME_MAX_CLIENT_UPDATES = 256 * 1024;
        constexpr size_t ME_MAX_MARKET_UPDATES = 256 * 1024;
        constexpr size_t ME_MAX_NUM_CLIENTS = 256;
        constexpr size_t ME_MAX_ORDER_IDS = 1024 * 1024;
        constexpr size_t ME_MAX_PRICE_LEVELS = 256;

    } // Common

}