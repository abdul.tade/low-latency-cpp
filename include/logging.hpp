#pragma once
#include <string>
#include <fstream>
#include <cstdio>
#include <cstring>
#include "macros.hpp"
#include "lock_free_queue.hpp"
#include "thread_utils.hpp"
#include "time_utils.hpp"

namespace hft{
    namespace Common
    {
        constexpr size_t LOG_QUEUE_SIZE = 8 * 1024 * 1024;
        enum /*class*/ LogType : int8_t {
            CHAR = 0, INTEGER = 1,
            LONG_INTEGER = 2, LONG_LONG_INTEGER = 3,
            UNSIGNED_INTEGER = 4, UNSIGNED_LONG_INTEGER = 5,
            UNSIGNED_LONG_LONG_INTEGER = 6, FLOAT = 7,
            DOUBLE = 8, STRING = 9
        };

        struct LogElement {
            LogType type;
            union {
                char c;
                int i; long l; long long ll;
                unsigned u; unsigned long ul;
                unsigned long long ull; float f;
                double d; const char* str;
            } u;
        };

        class Logger final  {
            const std::string file_name_;
            std::ofstream file_;
            LockFreeQueue<LogElement> queue_;
            std::atomic<bool> running_{true};
            std::thread *logger_thread_{nullptr};

        public:
            explicit Logger(const std::string &file_name)
                : file_name_(file_name),
                  queue_(LOG_QUEUE_SIZE)
            {
                file_.open(file_name);
                Macros::ASSERT(file_.is_open(), "Could not open log file: " + file_name);
                logger_thread_ = ThreadUtils::createAndStartThread(-1,"Common/Logger",
                    [this]() -> void {
                        flushQueue();
                });
                Macros::ASSERT(logger_thread_ != nullptr, "Failed to start Logger thread.");
            }

            Logger() = delete;
            Logger(const Logger &) = delete;
            Logger(const Logger &&) = delete;
            Logger &operator=(const Logger &) = delete;
            Logger &operator=(const Logger &&) =  delete;

            ~Logger() {
                std::cerr << "Flushing and closing debugger for "
                          << file_name_ << '\n';
                while (queue_.size())
                {
                    using namespace std::chrono_literals;
                    std::this_thread::sleep_for(1s);
                }
                running_ = false;
                logger_thread_->join();
                file_.close();
            }

            auto pushValue(const LogElement &log_element) noexcept {
                *(queue_.getNextToWriteTo()) = log_element;
                queue_.updateWriteIndex();
            }

            auto pushValue(char value) noexcept {
                pushValue(LogElement{LogType::CHAR, {.c = value}});
            }

            /* requires improvment*/
            auto pushValue(const char* value) noexcept {
                pushValue(LogElement{LogType::STRING, {.str = value}});
            }

            auto pushValue(const std::string &value) noexcept {
                pushValue(value.c_str());
            }

            auto pushValue(int value) noexcept {
                pushValue(LogElement{LogType::INTEGER, {.i = value}});
            }

            auto pushValue(long value) noexcept {
                pushValue(LogElement{LogType::LONG_INTEGER, {.l = value}});
            }

            auto pushValue(long long value) noexcept {
                pushValue(LogElement{LogType::LONG_LONG_INTEGER, {.ll = value}});
            }

            auto pushValue(unsigned value) noexcept {
                pushValue(LogElement{LogType::UNSIGNED_INTEGER, {.u = value}});
            }

            auto pushValue(unsigned long value) noexcept {
                pushValue(LogElement{LogType::UNSIGNED_LONG_INTEGER, {.ul = value}});
            }

            auto pushValue(unsigned long long value) noexcept {
                pushValue(LogElement{LogType::UNSIGNED_LONG_LONG_INTEGER,{.ull = value}});
            }

            auto pushValue(float value) noexcept {
                pushValue(LogElement{LogType::FLOAT, {.f = value}});
            }

            auto pushValue(double value) noexcept {
                pushValue(LogElement{LogType::DOUBLE, {.d = value}});
            }

            
            template <typename T, typename... Args>
            /* interpolation character is # */
            auto log(const char* str, const T &value, Args &&...args) noexcept {
                while (*str)
                {
                    if (*str == '#') {
                        if (UNLIKELY(*(str + 1) == '#')) {
                            ++str;
                        } else {
                            pushValue(value);
                            log(str+1, args...);
                            return;
                        }
                    }
                    pushValue(*str++);
                }
                Macros::FATAL("extra arguments provided to log()");
            }  

            /*the interpolation character is #*/
            auto log(const char* str) noexcept {
                if (findChar(str, '#')) {
                    Macros::FATAL("missing arguments to log()");
                }
                pushValue(str);
            }

        private:
            void flushQueue() noexcept {
                while (running_)
                {
                    for (auto next = queue_.getNextToRead(); queue_.size() && next; 
                        next = queue_.getNextToRead())
                    {
                        switch (next->type)
                        {
                            case LogType::CHAR:
                                file_ << next->u.c;
                                break;
                            case LogType::INTEGER:
                                file_ << next->u.i;
                                break;
                            case LogType::LONG_INTEGER:
                                file_ << next->u.l;
                                break;
                            case LogType::LONG_LONG_INTEGER:
                                file_ << next->u.ll;
                                break;
                            case LogType::UNSIGNED_INTEGER:
                                file_ << next->u.u;
                                break;
                            case LogType::UNSIGNED_LONG_INTEGER:
                                file_ << next->u.ul;
                                break;
                            case LogType::UNSIGNED_LONG_LONG_INTEGER:
                                file_ << next->u.ull;
                                break;
                            case LogType::FLOAT:
                                file_ << next->u.f;
                                break;
                            case LogType::DOUBLE:
                                file_ << next->u.d;
                                break;
                            case LogType::STRING:
                                file_ << next->u.str;
                                break;
                        }
                        queue_.updateReadIndex();
                        next = queue_.getNextToRead();
                    }
                    using namespace std::chrono_literals;
                    std::this_thread::sleep_for(1ms);
                }
            }

            bool findChar(const char* str, char c) {
                return std::strchr(str, c) != nullptr;
            }
        };
    } // namespace Common
    
} // namespace hft
