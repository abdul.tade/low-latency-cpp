#pragma once
#include <functional>
#include "socket_utils.hpp"
#include "logging.hpp"

namespace hft
{
    namespace Common
    {
        constexpr size_t TCPBufferSize = 256 * 1024 * 1024;
        struct TCPSocket
        {

            explicit TCPSocket(Logger &logger)
                : logger_(logger)
            {
                send_buffer_ = new char[TCPBufferSize];
                rcv_buffer_ = new char[TCPBufferSize];
                recv_callback_ = [this](auto socket, auto rx_time)
                {
                    defaultRecvCallback(socket, rx_time);
                };
            }

            ~TCPSocket()
            {
                destroy();
                delete[] send_buffer_;
                delete[] rcv_buffer_;
                send_buffer_ = nullptr;
                rcv_buffer_ = nullptr;
            }

            TCPSocket() = delete;
            TCPSocket(const TCPSocket &) = delete;
            TCPSocket(const TCPSocket &&) = delete;
            TCPSocket &operator=(const TCPSocket &) = delete;
            TCPSocket &operator=(TCPSocket &&) = delete;

            void destroy() noexcept
            {
                if (fd_ != -1)
                    close(fd_);
            }

            int connect(const std::string &ip, const std::string &iface, int port, bool is_listening)
            {
                destroy();
                fd_ = createSocket(logger_, ip, iface, port, false, false, is_listening, 100, true);
                inAddr.sin_addr.s_addr = INADDR_ANY;
                inAddr.sin_port = htons(port);
                inAddr.sin_family = AF_INET;
                return fd_;
            }

            void send(const void *data, size_t len) noexcept
            {
                if (len > 0)
                {
                    std::memcpy(send_buffer_ + next_send_valid_index_, data, len);
                    next_send_valid_index_ += len;
                }
            }

            bool sendAndRecv() noexcept
            {
                char ctrl[CMSG_SPACE(sizeof(struct timeval))];
                cmsghdr *cmsg = (cmsghdr *)&ctrl;
                iovec iov;
                iov.iov_base = rcv_buffer_ + next_rcv_valid_index_;
                iov.iov_len = TCPBufferSize - next_rcv_valid_index_;

                msghdr msg;
                msg.msg_control = ctrl;
                msg.msg_controllen = sizeof(ctrl);
                msg.msg_name = &inAddr;
                msg.msg_namelen = sizeof(inAddr);
                msg.msg_iov = &iov;
                msg.msg_iovlen = 1;

                const auto n_rcv = recvmsg(fd_, &msg, MSG_DONTWAIT);
                if (n_rcv > 0)
                {
                    next_rcv_valid_index_ += n_rcv;
                    Nanos kernel_time = 0;
                    timeval time_kernel;
                    if (cmsg->cmsg_level == SOL_SOCKET && cmsg->cmsg_type == SCM_TIMESTAMP && cmsg->cmsg_len == CMSG_LEN(sizeof(time_kernel)))
                    {
                        std::memcpy(&time_kernel, CMSG_DATA(cmsg), sizeof(time_kernel));
                        kernel_time = (time_kernel.tv_sec * NANOS_TO_SECS) + (time_kernel.tv_usec * NANOS_TO_MICROS);
                    }

                    const auto user_time = TimeUtils::getCurrentNanos();
                    logger_.log("#:# #() # read socket:# len:# utime:# ktime:# diff:#\n",
                                __FILE__, __LINE__, __FUNCTION__, TimeUtils::getCurrentTimeStr(&time_str_),
                                fd_, next_rcv_valid_index_, user_time, kernel_time, (user_time - kernel_time));
                    recv_callback_(this, kernel_time);
                }

                ssize_t n_send = std::min(TCPBufferSize, next_send_valid_index_);
                while (n_send > 0)
                {
                    auto n_send_this_msg = std::min(static_cast<ssize_t>(next_send_valid_index_), n_send);
                    const int flags = MSG_DONTWAIT | MSG_NOSIGNAL |
                                      (n_send_this_msg < n_send ? MSG_MORE : 0);
                    auto n = ::send(fd_, send_buffer_, n_send_this_msg, flags);

                    if (UNLIKELY(n < 0))
                    {
                        if (!wouldBlock())
                            send_disconnected_ = true;
                        break;
                    }
                    logger_.log("#:#:#() # send_socket:# len:#\n",
                                __FILE__, __LINE__, __FUNCTION__, TimeUtils::getCurrentTimeStr(&time_str_), fd_, n);
                    n_send -= n;
                    Macros::ASSERT(n == n_send_this_msg, "Don't support partial send lengths lengths yet.");
                }
                next_send_valid_index_ = 0;
                return (n_rcv > 0);
            }

            int fd_{-1};
            char *send_buffer_{nullptr};
            size_t next_send_valid_index_{0};
            char *rcv_buffer_{nullptr};
            size_t next_rcv_valid_index_{0};
            bool send_disconnected_{false};
            bool recv_disconnected_{false};
            struct sockaddr_in inAddr;
            std::function<void(TCPSocket *, Nanos)> recv_callback_;
            std::string time_str_;
            Logger &logger_;

            void defaultRecvCallback(TCPSocket *socket, Nanos rx_time) noexcept
            {
                logger_.log("#:# #() # TCPSocket::defaultRecvCallback() socket:# len:# rx:#\n",
                            __FILE__, __LINE__, __FUNCTION__, TimeUtils::getCurrentTimeStr(&time_str_),
                            socket->fd_, socket->next_rcv_valid_index_, rx_time);
            }
        };
    } // namespace Common

} // namespace hft
namespace Common
{

} // namespace Common
