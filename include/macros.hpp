#pragma once
#include <iostream>

#define LIKELY(x) __builtin_expect(!!(x), 1)
#define UNLIKELY(x) __builtin_expect(!!(x), 0)

namespace hft {

    struct Macros
    {
        static inline auto ASSERT(bool cond, const std::string &msg) noexcept {
            if (UNLIKELY(!cond)) {
                std::cerr << msg << '\n';
                exit(EXIT_FAILURE);
            }
        }

        static inline auto FATAL(const std::string &msg)
        {
            std::cerr << msg << '\n';
            exit(EXIT_FAILURE);
        }
    };

}