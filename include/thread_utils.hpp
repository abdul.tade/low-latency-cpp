#pragma once

#include <iostream>
#include <atomic>
#include <thread>
#include <unistd.h>
#include <sys/syscall.h>

namespace hft
{

    struct ThreadUtils
    {
        template <typename Func, typename... Args>
        static inline auto createAndStartThread(
            int core_id, const std::string &name, Func &&func, Args &&...args) noexcept
        {  
            std::atomic<bool> running{false}, failed{false};
            auto threadBody = [&]() mutable {
                if (core_id >= 0 && !ThreadUtils::setThreadCore(core_id)) {
                    std::cerr << "Failed to set core affinity for "
                              << name << " " << pthread_self() << " to "
                              << core_id << '\n';
                    failed = true;
                    return;
                }
                std::cout << "Set core affinity for " << name << " tid: "
                          << pthread_self() << " to id: " << core_id << '\n';
                running = true;
                func(std::forward<Args>(args) ...);
            };
            auto t = new std::thread{threadBody};
            while (!running && !failed) {
                using namespace std::chrono_literals;
                std::this_thread::sleep_for(1s);
            }

            if (failed) {
                t->join();
                delete t;
                t = nullptr;
            }

            return t;
        }

        static inline bool setThreadCore(int core_id) noexcept {
            cpu_set_t cpuset;
            CPU_ZERO(&cpuset);
            CPU_SET(core_id,&cpuset);
            return pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), &cpuset) == 0;
        }
    };
}