CXX := g++
CXXFLAGS := -O3 -Wall -std=c++20

.PHONY: all build run

all:
	make build
	make run


build: src/main.cpp include/ build/ 
	$(CXX) $(CXXFLAGS) -o build/main src/main.cpp -fno-rtti -march=native -flto -fwhole-program -fno-exceptions 


run: build/main
	sudo ./build/main

build-debug: src/main include/ build/
	$(CXX) $(CXXFLAGS) -g -o build/main src/main.cpp -fno-rtti -march=native -flto -fwhole-program -fno-exceptions

debug: build/main
	gdb -q build/main