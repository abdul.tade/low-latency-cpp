#include "../include/time_utils.hpp"
#include "../include/logging.hpp"
#include "../include/tcp_socket.hpp"
#include "../include/tcp_server.hpp"

int main(int, char **)
{
	using namespace hft::Common;
	std::string time_str_;
	Logger logger_("socket_example.log");
	auto tcpServerRecvCallback = [&](TCPSocket *socket, Nanos
															rx_time) noexcept
	{
		logger_.log("TCPServer::defaultRecvCallback() socket : % len : % rx : %\n ",
					socket->fd_,
					socket->next_rcv_valid_index_, rx_time);
		const std::string reply = "TCPServer received msg:" +
								  std::string(socket->rcv_buffer_, socket->next_rcv_valid_index_);
		socket->next_rcv_valid_index_ = 0;
		socket->send(reply.data(), reply.length());
	};
	auto tcpServerRecvFinishedCallback = [&]() noexcept
	{
		logger_.log("TCPServer::defaultRecvFinishedCallback()\n");
	};
	auto tcpClientRecvCallback = [&](TCPSocket *socket, Nanos
															rx_time) noexcept
	{
		const std::string recv_msg = std::string(socket->rcv_buffer_, socket->next_rcv_valid_index_);
		socket->next_rcv_valid_index_ = 0;
		logger_.log("TCPSocket::defaultRecvCallback() socket : % len : % rx : % msg : %\n ",
					socket->fd_,
					socket->next_rcv_valid_index_, rx_time,
					recv_msg);
	};
}